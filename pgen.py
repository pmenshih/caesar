"""
Скрипт генерации паролей по заданному списку компонентов.

Паттерн пароля ABABA состоит из комбинации компонентов А и В, которые представляют собой наборы частей пароля.
"""

import itertools
import os
import sys

def main():
    """
    Основная функция приложения.

    При запуске ожидает дополнительный параметр с путем к файлу компонентов пароля. Файл разбит на две группы компонентов, отделенных строчными префиксами ===A=== и ===B===.

    Результат работы - построчный вывод в STDOUT сгенерированных паролей.
    """
    # Разбор параметров командной строки. Валидация параметров не производится.
    if len(sys.argv) != 2:
        print('Неверное количество параметров.')
        print('python pgen.py [файл_с_компонентами_паролей]')
        exit()
    p_parts_filename = sys.argv[1]

    # Загрузка компонентов паролей.
    if not os.path.isfile(p_parts_filename):
        print('Неверное имя или путь к файлу с компонентами паролей')

    parts_a = []
    parts_b = []
    cur_part = ''
    # Чтение файла компонентов пароля.
    for line in open(p_parts_filename, 'r').readlines():
        line = line.rstrip('\n')
        if line == '===A===':
            cur_part = 'A'
            continue
        elif line == '===B===':
            cur_part = 'B'
            continue
        if cur_part == 'A':
            parts_a.append(line)
        elif cur_part == 'B':
            parts_b.append(line)

    combos = list(itertools.product(
        range(len(parts_a)),
        range(len(parts_b)),
        range(len(parts_a)),
        range(len(parts_b)),
        range(len(parts_a))
        ))

    for combo in combos:
        print(''.join([
            parts_a[combo[0]],
            parts_b[combo[1]],
            parts_a[combo[2]],
            parts_b[combo[3]],
            parts_a[combo[4]],
            ]))

if __name__ == "__main__":
    main()
