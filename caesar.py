"""
Брутфорсер паролей для LUKS-образов
"""

import os
import sys

import pexpect

def print_progress_bar(
        iteration,
        total,
        prefix='',
        suffix='',
        decimals=1,
        length=50,
        fill='█'
        ):
    """
    https://stackoverflow.com/a/34325723
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filled_length = int(length * iteration // total)
    prog_bar = fill * filled_length + '-' * (length - filled_length)
    print(f'\r{prefix} |{prog_bar}| {percent}%% {suffix}', end='\r')
    # Print New Line on Complete
    if iteration == total:
        print()

def main():
    """
    Основная функция приложения.

    При запуске ожидает 3 дополнительных параметра командной строки (порядок следования важен):
        - имя устройства для монтирования;
        - имя образа для cryptsetup;
        - путь к файлу со списком паролей для перебора.
    """
    progress_filename = 'progress.txt'

    # Разбор параметров командной строки. Валидация параметров не производится.
    if len(sys.argv) != 4:
        print('Неверное количество параметров.')
        print('python caesar.py [имя_устройства] [имя_образа] [файл_паролей]')
        exit()
    dev_name = sys.argv[1]
    im_name = sys.argv[2]
    pwd_filename = sys.argv[3]

    # Загрузка паролей для подбора.
    if not os.path.isfile(pwd_filename):
        print('Неверное имя или путь к файлу с паролями')
    pwds = []
    with open(pwd_filename, 'r') as pwd_file:
        pwds = pwd_file.read().splitlines()
    passwords_total = len(pwds)
    print(f'Паролей загружено: {passwords_total}')

    # Открытие файла,
    # в котором содержится номер последнего попробованного пароля.
    seek = 0
    if os.path.isfile(progress_filename):
        prog_file = open(progress_filename, 'r')
        tmp_seek = prog_file.read().rstrip('\n')
        if tmp_seek.isdigit():
            tmp_seek = int(tmp_seek)
            if tmp_seek and tmp_seek < len(pwds):
                print(f'Подбор будет продолжен со строки {tmp_seek}.')
                seek = tmp_seek-1
                # Срез списка паролей до нужного количества.
                pwds = pwds[seek:]
        prog_file.close()
    prog_file = open(progress_filename, 'w')

    # Отмонтирование устройства. На всякий случай.
    pexpect.run(f'umount {dev_name}')
    # Закрытие устройства образа. На всякий случай.
    pexpect.run(f'cryptsetup close {im_name}')

    # Команда для запуска cryptsetup.
    cmd = f'cryptsetup open --type luks {dev_name} {im_name} -T {len(pwds)}'
    proc = pexpect.spawn(cmd)

    # Прогресс-бар.
    print_progress_bar(seek, passwords_total, length=50)

    for pwd in pwds:
        # Ожидаем приглашение к вводу пароля.
        proc.expect(':')
        # Отправляем очередной пароль из списка.
        proc.sendline(pwd)
        # Ждем или выхода из программы (успех) или фразы начинающейся с No key.
        idx = proc.expect([pexpect.EOF, 'No key'])
        # Если случилось первое, то пароль найден.
        if idx == 0:
            # Печатаем его в STDOUT и выходим.
            print(f'\nCONGRAT! PASSWORD IS: {pwd}')
            exit()
            # Закрытие устройства образа.
            pexpect.run(f'cryptsetup close {im_name}')
        # Запись индекса обработанного пароля.
        prog_file.seek(0)
        seek += 1
        prog_file.write(str(seek))
        # Обновление прогресс-бара.
        print_progress_bar(seek, passwords_total, length=50)

if __name__ == "__main__":
    main()
